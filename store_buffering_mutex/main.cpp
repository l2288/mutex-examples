#include <iostream>
#include <thread>

//////////////////////////////////////////////////////////////////////

// Prevents compiler reordering
inline void CompilerBarrier() {
  asm volatile("" ::: "memory");
}

//////////////////////////////////////////////////////////////////////

class Mutex2 {
 public:
  Mutex2() {
    want_[0] = false;
    want_[1] = false;
    victim_ = 0;
  }

  void Lock(size_t thread_index) {
    want_[thread_index] = true;
    CompilerBarrier();
    victim_ = thread_index;
    CompilerBarrier();
    while (want_[1 - thread_index] && victim_ == thread_index) {
       ; // Backoff
    }
  }

  void Unlock(size_t thread_index) {
    want_[thread_index] = false;
  }

 private:
  bool want_[2];
  size_t victim_;
};

//////////////////////////////////////////////////////////////////////

void MaybeStoreBuffering(size_t iter) {
  Mutex2 mutex;
  size_t cs = 0;

  std::thread t0([&]() {
    mutex.Lock(0);
    ++cs;
    mutex.Unlock(0);
  });

  std::thread t1([&]() {
    mutex.Lock(1);
    ++cs;
    mutex.Unlock(1);
  });

  t0.join();
  t1.join();

  if (cs != 2) {
    std::cout << "Iteration #" << iter << ": Peterson lock is broken =(" << std::endl;
    std::abort();
  }
}

//////////////////////////////////////////////////////////////////////

int main() {
  for (size_t i = 0; ; ++i) {
    MaybeStoreBuffering(i);
    if (i % 10000 == 0) {
      std::cout << "Iterations made: " << i + 1 << std::endl;
    }
  }
  return 0;
}
